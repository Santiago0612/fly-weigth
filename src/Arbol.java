public class Arbol implements ArbolLigero{
    public String tipo;

    public Arbol(String tipo){
        this.tipo=tipo;
    }

    @Override
    public String getTipo() {
        return this.tipo;
    }

    @Override
    public void Dibujar(long x, long y, long z) {
        System.out.println("arbol [" + this.getTipo()+"]");
    }

}
