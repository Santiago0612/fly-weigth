import java.util.HashMap;
import java.util.Map;
import java.lang.Object;
public class FabricaDeArboles {
    private Map<String, ArbolLigero> arboles;

    public FabricaDeArboles(){
        this.arboles=new HashMap();
    }
    public ArbolLigero getArbol(String tipo){
        if(!arboles.containsKey(tipo)){
            arboles.put(tipo, new Arbol(tipo));
        }
        return arboles.get(tipo);
    }
}
